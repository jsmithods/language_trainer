import React, { Component } from 'react';
import './English.css';
import { Link } from "react-router-dom";

class English extends Component {
  render() {
    const localNav = <div className="english_nav">
      <ul>
        <li>
          <Link to={`${this.props.match.url}/ielts`}>IELTS</Link>
        </li>
        <li>
          <Link to={`${this.props.match.url}/vocabulary`}> Vocabulary </Link>
        </li>
      </ul>
    </div>;

    return <div>
      <h3>English</h3>
      {localNav}
    </div>;

  }
}

export default English;