import React, { Component } from 'react';
import './Vocabulary.css';
import { Link } from "react-router-dom";

// Import Data file
import vocabData from '../../data_source/vocabulary_data';

class Vocabulary extends Component {
  state = {
    vocabulary: {}
  };

  componentWillMount() {
    this.setState({vocabulary: vocabData})
  }

  render() {
    const courses_list = this.state.vocabulary.courses;

    const showCoursesList = courses_list.map(course => {
      const courseWordsArray = course.lessons.map(lesson => lesson.words.length);
      const wordsCount = courseWordsArray.reduce((accumulator, currentValue) => accumulator + currentValue);
      return <li>
        <Link to={`${this.props.match.url}/${course.url}`}>{course.name}</Link>
        <p>{wordsCount} words</p>
      </li>
    });

    const localNav = <div className="english_nav">
      <ul>
        {showCoursesList}
      </ul>
    </div>;

    return <div className="vocabulary_container">
        <h2>English Vocabulary</h2>
          {localNav}
      </div>;
  }
}

export default Vocabulary;