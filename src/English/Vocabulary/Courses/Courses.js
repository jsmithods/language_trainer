import React, { Component } from 'react';
import { Link } from "react-router-dom";

// Import Data file
import vocabData from '../../../data_source/vocabulary_data';

class Courses extends Component {
  state = {
    dynamicCourses: [],
    groupedCourses: []
  };

  componentWillMount() {
    const currentLessons = vocabData.courses.filter(course => course.url === this.props.match.params.courseId)[0].lessons;
    this.setState({dynamicCourses: currentLessons});
  };

  render() {
    const goBackButton = <div className="custom_go_back"><Link to="/english/vocabulary">Go Back</Link></div>;

    const renderDynamicList = <React.Fragment>
      {this.state.dynamicCourses.map(course => <div className="lesson_list_item" key={course.id}>
        <Link to={`${this.props.match.url}/${course.url}`}>{course.name} </Link>
        <p>{course.words.length} Words</p>
      </div> )}
    </React.Fragment>;

    return <React.Fragment>
      {goBackButton}
      {/*<h2>Courses</h2>*/}
      {renderDynamicList}
    </React.Fragment>;
  }
}

export default Courses;