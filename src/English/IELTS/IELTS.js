import React, { Component } from 'react';
import { Link } from "react-router-dom";

class IELTS extends Component {
  render() {
    const goBackButton = <div className="custom_go_back"><Link to="/english">Go Back</Link></div>;
    const localNav = <div className="english_nav">
      <ul>
        <li>
          <Link to={`${this.props.match.url}/writing`}>Writing</Link>
        </li>
        <li>
          <Link to={`${this.props.match.url}/speaking`}>Speaking</Link>
        </li>
      </ul>
    </div>;

    return <React.Fragment>
      {goBackButton}
      <h3>Choose a module</h3>
      {localNav}
    </React.Fragment>;
  }
}

export default IELTS;