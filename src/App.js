import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// import routing components
import Home from './Home/Home';
import Results from "./Results/Results";

// English imports
import English from './English/English';
import Vocabulary from "./English/Vocabulary/Vocabulary";
import Courses from "./English/Vocabulary/Courses/Courses";
import LessonTypes from "./English/Vocabulary/LessonTypes/LessonTypes";
import LessonTypesSwitcher from "./English/Vocabulary/LessonTypes/LessonTypesSwitcher/LessonTypesSwitcher";
import IELTS from "./English/IELTS/IELTS";
import Writing from "./English/IELTS/Writing/Writing";
import WritingResolver from "./English/IELTS/Writing/WritingResolver/WritingResolver";
import Speaking from "./English/IELTS/Speaking/Speaking";
import SpeakingResolver from "./English/IELTS/Speaking/SpeakingResolver/SpeakingResolver";

// French imports
import French from './French/French';
import FrenchVocabulary from "./French/Vocabulary/Vocabulary";
import FrenchCourses from "./French/Vocabulary/Courses/Courses";
import FrenchLessonTypes from "./French/Vocabulary/LessonTypes/LessonTypes";
import FrenchLessonTypesSwitcher from "./French/Vocabulary/LessonTypes/LessonTypesSwitcher/LessonTypesSwitcher";

import Conjugation from './French/Conjugation/Conjugation';
import ConjugationSingleWord from './French/Conjugation/ConjugationSingleWord/ConjugationSingleWord';

class App extends Component {

  render() {

    const routingLinks = <ul className="navigation_top">
      <li>
        <Link to="/">Home</Link>
      </li>
    </ul>;

    const routingFunctions = <div>
      <Route exact path="/" component={Home} />

      {/* English Routes */}
      <Route exact path="/english" component={English} />
      <Route exact path={'/english/ielts'} component={IELTS} />
      <Route exact path={'/english/ielts/writing'} component={Writing} />
      <Route exact path={'/english/ielts/speaking'} component={Speaking} />
      <Route exact path={'/english/ielts/writing/:course'} component={WritingResolver} />
      <Route exact path={'/english/ielts/speaking/:course'} component={SpeakingResolver} />

      <Route exact path={'/english/vocabulary'} component={Vocabulary} />
      <Route exact path={'/english/vocabulary/:courseId'} component={Courses} />
      <Route exact path={'/english/vocabulary/:courseId/:unitId'} component={LessonTypes} />
      <Route exact path={'/english/vocabulary/:courseId/:unitId/:lessonType'} component={LessonTypesSwitcher} />

      {/* French Routes */}
      <Route exact path="/french" component={French} />
      <Route exact path={'/french/vocabulary'} component={FrenchVocabulary} />
      <Route exact path={'/french/vocabulary/:courseId'} component={FrenchCourses} />
      <Route exact path={'/french/vocabulary/:courseId/:unitId'} component={FrenchLessonTypes} />
      <Route exact path={'/french/vocabulary/:courseId/:unitId/:lessonType'} component={FrenchLessonTypesSwitcher} />

      <Route exact path={'/french/conjugation'} component={Conjugation} />
      <Route exact path={'/french/conjugation/:wordId'} component={ConjugationSingleWord} />

      {/* Results Routes */}
      <Route exact path="/results" component={Results} />
    </div>;

    return (
      <div className="App">
        <Router>
          <React.Fragment>
            {routingLinks}
            {routingFunctions}
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;
