import React, { Component } from 'react';
import './Home.css';
import { Link } from "react-router-dom";

class Home extends Component {
  render() {
    const homeNav = <div className="english_nav">
      <ul>
        <li>
          <Link to={'/english'}>English</Link>
        </li>
        <li>
          <Link to={'/french'}>French</Link>
        </li>
      </ul>
    </div>;
    return <div className="home_container">
      <h2>Choose your language</h2>
      {homeNav}
      <h3><Link to="/results">Results</Link></h3>
    </div>;
  }
}

export default Home;